#include <stdio.h>

int main(int argc, char *argv[]){
	if (argc < 2) {
		printf("Usage: ./scanner 192.168.0.1 443");
	} else {
		printf("Scanning ...\n");
		printf("Host: %s\n", argv[1]);
		printf("Port: %s\n", argv[2]); 
	}
	return 0;
}
